<div class="highlights row">

    <!-- Title section -->
    <div class="highlights-title col-lg-12">
        
        <h1>
        Servicios más solicitados
        </h1>
    
    </div>
    
    <!-- Sections -->
    <div class="highlights-section offset-lg-1 col-lg-3">
        
        <img src="img/dolphin_service.jpg" alt="dolphin_service">
        
        <h3>
        Limusina privada
        </h3>
        
        <p>
        Contrate los servicios de una nave Dolphin
        completamente equipada para una velocidad punta y confort completo.
        Dispone de todas las necesidades para viajes largos como cortos y pueden
        solicitarse para usos temporales o permanentes.
        </p>

        <a href="#">
            <button type="button" class="btn btn-secondary">
                Especificaciones
            </button>
        </a>
        
    </div>
    
    <div class="highlights-section offset-lg-1 col-lg-3">
    
        <img src="img/beluga_service.jpg" alt="beluga_service">

        <h3>
        Crucero privado
        </h3>
        
        <p>
        Contrate los servicios de una nave Beluga que cubre todos los requisitos
        de un crucero intergaláctico de última generación. Celebre cualquier fiesta o evento en un entorno a la altura.
        Dispone de servicios adicionales que pueden añadirse al contrato.
        </p>

        <a href="#">
            <button type="button" class="btn btn-secondary">
                Especificaciones
            </button>
        </a>	
    
    </div>
    
    <div class="highlights-section offset-lg-1 col-lg-3">
    
        <img src="img/fuelrat_service.jpg" alt="fuelrat_service">

        <h3>
        Fuelrat
        </h3>
        
        <p>
        Contrate la asistencia  de una rata de combustible, que irá inmediatamente
        desde la estación disponible más cercana a su ubicación, tardando tiempos récords en 
        llegar a su destino y llenar el depósito. Disponibles en todos los sistemas colonizados.
        </p>

        <a href="#">
            <button type="button" class="btn btn-secondary">
                Especificaciones
            </button>
        </a>

    </div>

    <div class="highlights-section offset-lg-1 col-lg-3">
        
        <img src="img/exploration_service.png" alt="exploration_service">
        
        <h3>
        Búsqueda planetaria
        </h3>
        
        <p>
        Ante la necesidad de nuevas colonias, nuevos recursos o lugares de 
        expansión planetaria, en Arcturian Enterprises ofrecemos servicios
        personalizables y especializados de exploración para encontrar con
        exactitud tipos de planetas que cumplan sus necesidades.
        </p>

        <a href="#">
            <button type="button" class="btn btn-secondary">
                Especificaciones
            </button>
        </a>
        
    </div>
    
    <!-- Possible service to put in -->
    <!-- 
    <div class="highlights-section offset-lg-1 col-lg-3">
    
        <img src="img/colony_transport.jpg" alt="Viaje a Colonia">

        <h3>
        Viaje a Colonia
        </h3>
        
        <p>
        Ofrecemos viajes programados a niveles muy económicos al sistema
        <b>Eol Prou RS-T d3-94</b>, donde se encuentra la única colonia humana a 
        22,000 años luz de la Burbuja. Nuestras naves Type-7 adecuadas para el
        transporte civil tienen el tiempo récord de llegada.
        </p>

        <a href="#">
            <button type="button" class="btn btn-secondary">
                Especificaciones
            </button>
        </a>

    </div> 
    -->

</div>