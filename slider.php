<div class="row">
				
    <div id="slider1" class="carousel slide" data-ride="carousel">
        
        <ol class="carousel-indicators">
            
            <li data-target="#slider1" data-slide-to="0" class="active"></li>
            <li data-target="#slider1" data-slide-to="1"></li>
            <li data-target="#slider1" data-slide-to="2"></li>
        
        </ol>
        
        <!-- div that contains the 3 sliders -->
        <div class="carousel-inner">
        
            <div class="carousel-item active">
                
                <div class="slider row">
                    
                    <!-- 
                    Possible reason why it does not work: 
                    https://stackoverflow.com/questions/28650327/responsive-images-srcset-not-working 
                    -->
                    <img class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" src="img/private_slider.jpg" 
                    sizes="100vw" srcset="img/private_slider_little.jpg 100w, img/private_slider_little.jpg 200w,
                    img/private_slider_little.jpg 400w, img/private_slider.jpg 800w,
                    img/private_slider.jpg 1000w, img/private_slider.jpg 1400w,
                    img/private_slider.jpg 1800w"  alt="Transporte privado">
                    
                    <div class="slider-text col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    
                        <h2>
                        Transporte privado
                        </h2>
                        
                        <h3 class="offset-lg-4 col-lg-4">
                        Ofrecemos todo tipo de servicios de transporte privado,
                        tanto para grupos especializados como particulares de lujo.
                        Todas los modelos de nave de la marca <i>Saud Kruger</i> están disponibles para usos adaptables
                        a las necesidades del cliente.
                        </h3>
                    
                    </div>
                
                </div>
            
            </div>
            
            <div class="carousel-item">
                
                <div class="slider row">
                    
                    <img class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" src="img/merchandise_slider.png" 
                    sizes="100vw" srcset="img/merchandise_slider_little.png 100w, img/merchandise_slider_little.png 200w,
                    img/merchandise_slider_little.png 400w, img/merchandise_slider.png 800w,
                    img/merchandise_slider.png 1000w, img/merchandise_slider.png 1400w,
                    img/merchandise_slider.png 1800w" alt="Transporte de mercancías">
                    
                    <div class="slider-text col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    
                        <h2>
                        Transporte de mercancías
                        </h2>
                        
                        <h3 class="offset-lg-4 col-lg-4">
                        Desde servicios de correo a caravanas mercantiles y transporte industrial.
                        Gracias a las naves de la marca <i>Lakon Spaceways</i> somos capaces de cubrir
                        las necesidades de nuestros clientes a un nivel de especialización completamente personal.
                        </h3>
                    
                    </div>
                
                </div>
                
            </div>
            
            <div class="carousel-item">
                
                <div class="slider row">
                    
                    <img class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" src="img/security_slider.jpg" 
                    sizes="100vw" srcset="img/security_slider_little.jpg 100w, img/security_slider_little.jpg 200w,
                    img/security_slider_little.jpg 400w, img/security_slider.jpg 800w,
                    img/security_slider.jpg 1000w, img/security_slider.jpg 1400w,
                    img/security_slider.jpg 1800w" alt="Seguridad privada">
                    
                    <div class="slider-text col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    
                        <h2>
                        Seguridad privada
                        </h2>
                        
                        <h3 class="offset-lg-4 col-lg-4">
                        Desde servicios de escolta a escuadrones de defensa. Tanto para necesidad de entidades
                        particulares como para corporaciones o megapotencias. Nuestras licencias nos permiten un 
                        amplio uso de naves de la marca <i>Faulcon DeLacy</i> para lograr satisfacer las exigencias
                        de nuestros contratos.
                        </h3>
                    
                    </div>
                
                </div>
                
            </div>
        
        </div>
        
        <!-- Configuration of the arrows and the bars in the carousel -->
        <a class="carousel-control-prev" href="#slider1" role="button" data-slide="prev">
            
            <span class="carousel-control-prev-icon" aria-hidden="true">
            </span>
            
            <span class="sr-only">
            Previous
            </span>
        
        </a>
        
        <a class="carousel-control-next" href="#slider1" role="button" data-slide="next">
            
            <span class="carousel-control-next-icon" aria-hidden="true">
            </span>
            
            <span class="sr-only">
            Next
            </span>
        
        </a>
        
    </div>
    
</div>