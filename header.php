<body class="body">
		
    <div class="container-fluid">

        <div class="headboard row">
                        
            <div class="logo col-4 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                <img src="img/arcturian_enterprises_logo_text.png">
            </div>
            
            <!-- This is the bootstrap nav menu -->
            <div class="btn-group offset-0 offset-sm-0 offset-md-6 offset-lg-6 offset-xl-6 col-8 col-sm-3 
            col-md-3 col-lg-3 col-xl-3 h-25 d-inline-block" role="group" aria-label="Menu" style="z-index:2">
                
                <nav class="navbar navbar-expand-lg navbar-light">
                    
                    <a class="navbar-brand" href="https://inara.cz/wing/2078"></a>
                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" 
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    
                        <span class="navbar-toggler-icon"></span>
                        
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarNav">
                    
                        <!-- The class "nav-item" can change to "nav-link disabled" to disable the button -->
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Iniciar sesión</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Registrarse</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Servicios</a>
                            </li>
                        </ul>
                    
                    </div>
                
                </nav>
            
            </div>

    <!-- Menu only for mobile -->
    <!--
    <div class="only-mobile" id="only-mobile">
        
        <div class="menu-mobile" id="menu-mobile">

            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            
            <div class="menu-mobile-content">
                <a href="#">Inicio</a>
                <a href="#">Iniciar sesión</a>
                <a href="#">Registrarse</a>
                <a href="#">Servicios</a>
            </div>

        </div>

        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Abrir</span>

    </div>
    -->
    <!-- Fin menú móvil -->
    
</div>