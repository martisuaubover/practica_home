<!DOCTYPE html>

<html>
	
	<?php include("head.php"); ?>
    
    <body class="body">
		
		<div class="container-fluid">
		
		<?php include("header.php"); ?>

			<!-- Formulario -->
            <div class="form container">

				<div>
					<h1 class="sendalert"></h1>
				</div>

                <form id="registro" action="" method="POST">

                    <div class="form-group">

                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" id="name"  name="name" placeholder="Escriba su nombre">
                    
                    </div>

                    <div class="form-group">

                        <label for="lastname">Apellidos</label>
                        <input type="text" class="form-control" id="lastname"  name="lastname" placeholder="Escriba su apellido">
                    
                    </div>

                    <div class="form-group">

                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email"  name="email"  placeholder="Escriba su email">
                    
                    </div>

                    <div class="form-group">

                        <label for="email_check">Repita el Email</label>
                        <input type="email" class="form-control" id="email_check"  name="email_check"  placeholder="Repita su email">
                        
                    </div>

                    <div class="form-group">

                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password"  placeholder="Introduzca su contraseña">
                    
                    </div>

                    <div class="form-group">

                        <label for="password_check">Repita la Password</label>
                        <input type="password" class="form-control" id="password_check" name="password_check"  placeholder="Repita su contraseña">
                        
                    </div>

                    <div class="form-group">

                        <label for="faction">Facción afiliada</label>

                        <select class="select2" id="faction" name="faction">
                            <option value="Imp">Imperio</option>
                            <option value="Fed">Federación</option>
                            <option value="Ali">Alianza</option>
                            <option value="Ind">Independiente</option>
                        </select>
                    
					</div>
					
					<div class="form-group">

						<textarea name="editor1" id="editor1" rows="10" cols="80">
							
						</textarea>

					</div>

                    <input type="submit" id="submit" name="submit" value="Enviar"/>

				</form>
            
			</div>
			<!--
				- Datatable -
			<div>

				<table id="example" class="display" width="100%"></table>

			</div>
			-->
            
            <?php include("footer.php"); ?>
	
	</body>

</html>