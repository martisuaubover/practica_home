

<head>
		
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" 
integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

<!-- Browser tab data -->
<title>Arcturus Enterprises Services</title>
<link rel="shortcut icon" href="img/arcturian_enterprises_logo.png">

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/app.css">

</head>

