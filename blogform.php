<!DOCTYPE html>

<html>
	
	<?php include("head.php"); ?>
    
    <body class="body">
		
		<div class="container-fluid">
		
		<?php include("header.php"); ?>

			<!-- Formulario -->
            <div class="form container">

				<div>
					<h1 class="sendalert"></h1>
				</div>

                <form id="registro" action="" method="POST">

                    <div class="form-group">

                        <label for="title">Título</label>
                        <input type="text" class="form-control" id="title"  name="title" placeholder="Título del artículo">
                    
                    </div>

                    <div class="form-group">

                        <textarea name="editor1" id="editor1" rows="10" cols="80">
                            
                        </textarea>

                    </div>

                    <div class="form-group">

                        <label for="category">Categoría</label>

                        <select class="select2" id="category" name="category">
                            <option value="noticia">Noticia</option>
                            <option value="opinion">Opinión</option>
                            <option value="shitpost">Shitpost</option>
                            <option value="humor">Humor</option>
                        </select>
                    
					</div>

                    <input type="submit" id="submit" name="submit" value="Enviar"/>

				</form>
            
			</div>
            
            <?php include("footer.php"); ?>
	
	</body>

</html>