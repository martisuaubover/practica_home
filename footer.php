        <!-- css and structure caught in http://www.java2s.com/Code/HTMLCSS/CSS-Controls/Footerwithcopyrightnotice.htm -->
        <div class="footer row">
                        
            <div class="offset-2 offset-lg-1 col-8 col-lg-2">
                &copy; 2018 Arcturus Alliance
            </div>
            
            <div class="offset-1 offset-lg-2 col-lg-2">
                Diseñado por <a href="https://www.twitch.tv/taoscuro/">Taoscuro</a> | <a href="http://www.cssportal.com/">CSS Portal </a>
            </div>
                
            <div class="offset-1 offset-lg-2 col-11 col-lg-3">
                
                <a href="index.html">Inicio</a> |
                <a href="index.html">Servicios</a> |
                <a href="index.html">RSS Feed</a> |
                <a href="http://validator.w3.org/check?uri=referer">XHTML</a> |
                <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>
                
            </div>
                    
        </div>			

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" 
    integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" 
    integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <!-- script for the mobile menu made for me -->
    <!--
    <script src="js/menu_mobile.js"></script>
    -->
    <!--
    <script>
        function openNav() {
            document.getElementById("menu-mobile").style.height = "100%";
        }
        
        function closeNav() {
            document.getElementById("menu-mobile").style.height = "0%";
        }
    </script>
    -->

</body>